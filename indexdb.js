try { // keep screen on if chrome device
	chrome.power.requestKeepAwake('display');
	chrome.app.window.current().fullscreen();
} catch(e) {};

/*/ put
(function () {
    var dbVersion = 1.0;
    var indexedDB = window.indexedDB;

    var request = indexedDB.open("VideoFiles", dbVersion);
    var db;
    var blob;

    function createObjectStore(dataBase) {
        dataBase.createObjectStore("Videos");
    }

    function getVideoFile() {
        var xhr = new XMLHttpRequest();
        var blob;
        
        xhr.open("GET", "/assets/img/PowerSchool-vw.mp4", true);
        xhr.responseType = "blob";
        xhr.addEventListener("load", function() {
            if (xhr.status === 200) {
                blob = xhr.response;
                putVideoInDb(blob);

                console.log(blob);
                var videoURL = URL.createObjectURL(blob);
        
                // Set video src to ObjectURL
                var videoElement = document.getElementById("videoPlayer");
                videoElement.setAttribute("src", videoURL);
            } else {
                console.log("error downloading video");
            }
        }, false);
        xhr.send();
    }

    function putVideoInDb(blob) {
        var transaction = db.transaction(["Videos"], "readwrite");
        var put = transaction.objectStore("Videos").put(blob, "savedvideo");
    }

    request.onerror = function (event) {
        console.log("error creating/accessing indexeddb");
    }

    request.onsuccess = function (event) {
        db = request.result;

        db.onerror = function (event) {
            console.log("error creating/accessing indexeddb");
        }
        getVideoFile();
    }

    request.onupgradeneeded = function (event) {
        createObjectStore(event.target.result);
    };
})();

// get
(function () {
    dbVersion = 1.0;
    var indexedDB = window.indexedDB;
 
    // Create/open database
    var request = indexedDB.open("VideoFiles");
     
    request.onerror = function (event) {
        // Failed to Open the indexedDB database
    };
 
    request.onsuccess = function (event) {
        db = request.result;
         
        // Open a transaction to the database
        var transaction = db.transaction(["Videos"], "readwrite");
 
        // Retrieve the video file
        transaction.objectStore("Videos").get("savedvideo").onsuccess = 
        function (event) {
            if (event.target.result) {
                var videoFile = event.target.result;
                console.log(videoFile);
                var videoURL = URL.createObjectURL(videoFile);
        
                // Set video src to ObjectURL
                var videoElement = document.getElementById("videoPlayer");
                videoElement.setAttribute("src", videoURL);
            }
        };
    }
})();*/