try { // keep screen on if chrome device
	chrome.power.requestKeepAwake('display');
	chrome.app.window.current().fullscreen();
} catch(e) {};

// https://stackoverflow.com/questions/4998908/convert-data-uri-to-file-then-append-to-formdata

function dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {type:mimeString});
}

// https://hacks.mozilla.org/2012/02/saving-images-and-files-in-localstorage/

chrome.storage.local.get("video", function(result) {
    var videoPlayer = document.getElementById("videoPlayer");
    var objectURL;
    if (result.video) {
        console.log("video");
        objectURL = URL.createObjectURL(dataURItoBlob(result.video));
        videoPlayer.setAttribute("src", objectURL);
    } else {
        console.log('no video')
        var xhr = new XMLHttpRequest(),
            fileReader = new FileReader();

        xhr.open("GET", "/assets/img/PowerSchool-vw.mp4", true);
        xhr.responseType = "blob";

        xhr.addEventListener("load", function() {
            if (xhr.status === 200) {
                fileReader.onload = function (evt) {
                    var result = evt.target.result;
                    objectURL = URL.createObjectURL(dataURItoBlob(result));
                    videoPlayer.setAttribute("src", objectURL);
                    try {
                        console.log("storage go");
                        //chrome.storage.local.set({"video": result});
                    } catch (e) {
                        console.log("Storage failed: " + e)
                    }
                };
                console.log("read go");
                fileReader.readAsDataURL(xhr.response);
            }
        }, false);
        xhr.send();
    }
});