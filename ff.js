    var videoStorage = localStorage.getItem("video");
    videoPlayer = document.getElementById("videoPlayer");

    if (videoStorage) {
        videoPlayer.setAttribute("src", videoStorage);
    } else {
        var xhr = new XMLHttpRequest(),
            blob,
            fileReader = new FileReader();

        xhr.open("GET", "http://info.expressimage.com/hubfs/1080p.mp4", true);
        xhr.responseType = "arraybuffer";

        xhr.addEventListener("load", function() {
            if (xhr.status === 200) {
                blob = new Blob([xhr.response], {type: "video/mp4"});

                fileReader.onload = function (evt) {
                    var result = evt.target.result;
                    videoPlayer.setAttribute("src", result);
                    try {
                        localStorage.setItem("video", result);
                    } catch (e) {
                        console.log("Storage failed: " + e)
                    }
                };
                fileReader.readAsDataURL(blob);
            }
        }, false);
        xhr.send();
    }